import polly from "polly-js"
import config from '../../config'

export function getMeasurments() {
    return polly()
        .retry(config.polly)
        .executeForPromise(() => {
            return fetch(`${config.url}/measurments`)
                .then(response => response.json());
        })
}

export function insertMeasurment(measurment: any) {
    return polly()
        .retry(config.polly)
        .executeForPromise(() => {
            return fetch(`${config.url}/insert/`, {
                method: `POST`,
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    data: measurment
                })
            })
        })
}