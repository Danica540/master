export function options(measurments: any) {
    return {
        credits: {
            enabled: false
        },
        chart: {
            type: 'spline',
        },
        title: {
            text: 'Live data'
        },
        xAxis: {
            type: 'datetime',
            title: {
                text: 'Date'
            }
        },
        yAxis: {
            title: {
                text: 'Metric'
            }
        },
        tooltip: {
            formatter: function () {
                if ((this as any).point.isNull) {
                    return 'Null';
                }
                // If not null, use the default formatter
                return "<b>" + (this as any).point.name + "</b><br>" + (this as any).point.y 
                + "<br>" + new Date((this as any).point.x)
            }
        },
        legend: {
            enabled: false
        },
        exporting: {
            enabled: false
        },
        series: [{
            data: measurments,
            color: '#0275d8'
        }]
    }
}