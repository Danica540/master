import 'process';

const config = {
    polly: process.env.POLLY_ENV ? parseInt(process.env.POLLY_ENV) : 5,
    url: process.env.URL_ENV || `http://localhost:3080`
};

export default config;