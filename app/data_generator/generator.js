const mysql = require(`mysql`);
const config = require(`./config.js`);

let counter = 0;

const connection = mysql.createConnection({
    host: config.get(`mysql.host`),
    user: config.get(`mysql.user`),
    password: config.get(`mysql.password`),
    database: config.get(`mysql.name`)
});

connection.connect(function (err) {
    if (err) {
        console.error(`[ERROR] error connecting: ${err.stack}`);
        return;
    }

    console.log(`[INFO] successfuly connected to mysql database`);
});

setInterval(() => {
    connection.query(`INSERT INTO measurements(Name,Value,Timestamp) VALUES ('metric${counter++}',${Math.random() * 800},${new Date().getTime()});`, (error, result, field) => {
        if (error) {
            console.error(`[ERROR] insert measurment: ${error.stack}`);
            return;
        }
        console.error(`[INFO] success insert measurments`);
    })
}, 5000)


process.on(`SIGTERM`, shutDown);
process.on(`SIGINT`, shutDown);

function shutDown() {
    process.exit();
}