const express = require(`express`);
const mysql = require(`mysql`);
const cors = require(`cors`);
const config = require(`./config.js`);

const port = config.get(`port`)
const app = express();

app.use(express.static(`public`));
app.use(express.urlencoded({ extended: true }));
app.use(express.json()) // To parse the incoming requests with JSON payloads

const corsOptions = {
    origin: `*`,
    credentials: true
}

app.use(cors(corsOptions))

let connections = [];

const connection = mysql.createConnection({
    host: config.get(`mysql.host`),
    user: config.get(`mysql.user`),
    password: config.get(`mysql.password`),
    database: config.get(`mysql.name`)
});

connection.connect(function (err) {
    if (err) {
        console.error(`[ERROR] error connecting: ${err.stack}`);
        return;
    }
    console.log(`[INFO] successfuly connected to mysql database`);
});

app.get(`/measurments`, (req, res) => {
    connection.query(`SELECT * FROM  measurements WHERE Timestamp>CAST((UNIX_TIMESTAMP() - 60000) AS decimal) ORDER BY Timestamp DESC LIMIT 100;`, (error, results, fields) => {
        if (error) {
            console.error(`[ERROR] get measurments: ${error.stack}`);
            return;
        }
        console.error(`[INFO] success get measurments`);
        res.json(results)
    })
})

app.post(`/insert`, (req, res) => {
    const data = req.body.data
    connection.query(`INSERT INTO measurements(Name,Value,Timestamp) VALUES ('${data.name}',${data.value},${data.timestamp});`, (error, results, fields) => {
        if (error) {
            console.error(`[ERROR] insert measurment: ${error.stack}`);
            return;
        }
        console.error(`[INFO] success insert measurments`);
    })
})

const server = app.listen(port, () => {
    console.log(`[INFO] Server running on port ${port}`);
});

server.on(`connection`, connection => {
    connections.push(connection);
    connection.on(`close`, () => connections = connections.filter(curr => curr !== connection));
});

process.on(`SIGTERM`, shutDown);
process.on(`SIGINT`, shutDown);

function shutDown() {
    console.log(`[INFO] Received kill signal, shutting down gracefully`);
    server.close(() => {
        console.log(`[INFO] Closed out remaining connections`);
        process.exit(0);
    });

    setTimeout(() => {
        console.error(`[ERROR] Could not close connections in time, forcefully shutting down`);
        process.exit(1);
    }, 10000);

    connections.forEach(curr => curr.end());
    setTimeout(() => connections.forEach(curr => curr.destroy()), 5000);
}